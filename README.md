# workstation-automation

Installing ansible on the target first:
```
sudo apt update
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
```

# how to run:
`ansible-playbook -vvv -i inventory playbook.yml`

## Todos
- possibly automate SSH key generation?
- Possibly automate GPG key generation?
<!-- - oh-my-zsh installation -->

https://www.debuntu.org/how-to-importexport-gpg-key-pair/