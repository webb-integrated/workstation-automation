#!/bin/bash
echo "starting"
mkdir .tmp
git clone https://github.com/facebook/watchman.git .tmp/watchman
cd .tmp/watchman
git checkout v4.9.0
./autogen.sh
./configure --without-pcre --enable-lenient
make
sudo make install