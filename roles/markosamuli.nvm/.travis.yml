---

language: generic
python: "2.7"

matrix:
  include:
    - name: "Ubuntu 16.04 (Xenial) with Ansible 2.4"
      os: linux
      dist: xenial
      sudo: required
      env: ANSIBLE_VERSION='<2.5.0'
    - name: "Ubuntu 16.04 (Xenial) with Ansible 2.5"
      os: linux
      dist: xenial
      sudo: required
      env: ANSIBLE_VERSION='<2.6.0'
    - name: "Ubuntu 16.04 (Xenial) with Ansible 2.6"
      os: linux
      dist: xenial
      sudo: required
      env: ANSIBLE_VERSION='<2.7.0'
    - name: "Ubuntu 16.04 (Xenial) with Ansible 2.7"
      os: linux
      dist: xenial
      sudo: required
      env: ANSIBLE_VERSION='<2.8.0'
    - name: "macOS 10.12 (Sierra) with Xcode 9.2"
      os: osx
      osx_image: xcode9.2
      env: BREW_UPGRADE=yes RVM_GPG_IMPORT=yes
    - name: "macOS 10.13 (High Sierra) with Xcode 10.1"
      os: osx
      osx_image: xcode10.1
    - name: "macOS 10.14 (Mojave) with Xcode 10.2 beta"
      os: osx
      osx_image: xcode10.2

# Install Ansible and Python development packages
addons:
  apt:
    packages:
      - python-pip
      - python-dev
  homebrew:
    packages:
      - ansible
      - pre-commit

branches:
  only:
    - master
    - develop
    - /^feature\/.*$/
    - /^bugfix\/.*$/
    - /^v\d+\.\d+(\.\d+)?(-\S*)?$/

cache:
  directories:
    - $HOME/.cache/pre-commit/

before_cache:
  - rm -f $HOME/.cache/pre-commit/pre-commit.log

before_install:
  # Import new GPG key for RVM when necessary
  - >
    if [[ "$RVM_GPG_IMPORT" == "yes" ]]; then
      command curl -sSL https://rvm.io/mpapis.asc | gpg --import -
    fi

  # Update Homebrew to fix HOMEBREW_LOGS error
  - if [[ "$BREW_UPGRADE" == "yes" ]]; then brew update; fi

  # Upgrade Ansible with installed Homebrew
  - if [[ "$BREW_UPGRADE" == "yes" ]]; then brew upgrade ansible; fi

  # Upgrade pre-commit installed with Homebrew
  - if [[ "$BREW_UPGRADE" == "yes" ]]; then brew upgrade pre-commit; fi

  # Cleanup existing NVM installation
  - unset NVM_CD_FLAGS
  - unset NVM_DIR
  - unset NVM_BIN
  - if [[ "$TRAVIS_OS_NAME" == "osx" ]]; then rm -rf /Users/travis/.nvm; fi
  - if [[ "$TRAVIS_OS_NAME" != "osx" ]]; then rm -rf /home/travis/.nvm; fi
  - rm -rf /etc/profile.d/travis-nvm.sh

install:
  # Install Ansible with pip on Ubuntu
  - >
    if [[ "$TRAVIS_OS_NAME" != "osx" ]]; then
      pip install --user ansible${ANSIBLE_VERSION}
    fi

  # Install pre-commit with pip on Ubuntu
  - >
    if [[ "$TRAVIS_OS_NAME" != "osx" ]]; then
      pip install --user pre-commit
    fi

  # Check Ansible version
  - ansible --version

  # Create ansible.cfg with correct roles_path
  - printf '[defaults]\nroles_path=../\n' > ansible.cfg

before_script:
  # https://github.com/travis-ci/travis-ci/issues/6307
  - if [[ "$TRAVIS_OS_NAME" == "osx" ]]; then rvm get head || true; fi

script:
  # Run pre-commit hooks
  - pre-commit run -a

  # Basic role syntax check
  - ansible-playbook tests/test.yml -i tests/inventory --syntax-check

  # Test role run
  - >
    ansible-playbook tests/test.yml -i tests/inventory --connection=local

  # Test idempotence
  - >
    ansible-playbook tests/test.yml -i tests/inventory --connection=local
    | grep -q 'changed=0.*failed=0'
    && (echo 'Idempotence test: pass' && exit 0)
    || (echo 'Idempotence test: fail' && exit 1)

notifications:
  webhooks: https://galaxy.ansible.com/api/v1/notifications/
